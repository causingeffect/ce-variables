# CE Variables

![CE Variables](https://docs.causingeffect.com/expressionengine/ce-variables/media/ce-variables.jpg)

**Smart, Efficient Variables**


## Documentation

For installation or upgrade instructions, please visit the [CE Variables documentation](https://docs.causingeffect.com/expressionengine/ce-variables/index.html).


## License

* Brought to you by Causing Effect (causingeffect.com), Aaron Waldon (aaron@causingeffect.com)
* [MIT License](http://opensource.org/licenses/mit-license.php)